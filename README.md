# RF-Track Jupyter Notebooks

These notebooks are available in two equivalent versions: Python and Octave. Check the prefix and choose the version you prefer.

Below is a description of each notebook. Their complexity increases with the notebook number, so we recommend following them in sequential order.

## Example 1
    Shows how to generate a particle bunch from an arbitrary beam matrix.

## Example 2
    Explains how to create a particle bunch from the Twiss parameters.

## Example 3
    Illustrates how to set up a FODO cell and track a particle bunch through it.

## Example 4
    Demonstrates how to match the Twiss parameters in a FODO cell.

## Example 5
    Explains how to set up a simple traveling-wave accelerating structure.

## Example 6
    Shows how to set up a simple traveling-wave accelerating structure using a 1D field map.

## Example 7
    Creates a basic X-band linac to accelerate particles from 100 MeV to 500 MeV.

## Example 8
    Examines jitter amplification in the X-band linac of Example 6.

## Example 9
    Shows the definition and application of short-range wakefields.

## Example 10
    Analyzes the impact of wakefields on the X-band linac and their role in jitter amplification.

## Example 11
    Explores methods for reducing energy spread in the X-band linac accelerating particles from 100 MeV to 500 MeV.

## Example 12
    Investigates the effects of static imperfections on the X-band linac and the implementation of beam-based alignment techniques.

## Example 13
    Simulates a photoinjector in Volume.

## Example 14
    Presents the design of a dipole magnet for a compact ion ring intended for medical applications (HITRIplus project).

## Example 15
    Analyzes transient and steady-state beam loading in a traveling-wave structure.

## Example 16
    Simulates the process of inverse-Compton scattering.
