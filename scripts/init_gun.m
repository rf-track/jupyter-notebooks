%% max_field in MV/m
%% phid in degrees

function [Gun,S0] = init_gun (setup)
    RF_Track;
    freq = 2.99855e9; % Hz
    T = load('data/GUN_ideal_SF_100.txt');
    S = T(:,1); % m
    L = range(S); % m
    S0 = min(S); % m
    S1 = max(S); % m
    Ez = T(:,2) * setup.Ez * 1e6; % V/m
    hz = range(S) / (length(S)-1); % m

    Gun = RF_FieldMap_1d (Ez, hz, L, freq, +1);
    Gun.set_t0(0.0);
    Gun.set_phid(setup.PHID);
