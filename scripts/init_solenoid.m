% max_field in T

function [Solenoid,S0] = init_solenoid (Setup)
    RF_Track;
    T = load('data/profile_califes_bon_B1_285_B3_338.txt');
    S = T(:,1); % m
    S0 = min(S); % m
    S1 = max(S); % m
    Bz = T(:,2); %
    Bz = Bz * Setup.Bz; % T
    hz = range(S) / (length(S)-1); % m

    Solenoid = Static_Magnetic_FieldMap_1d (Bz, hz);