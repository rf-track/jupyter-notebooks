function B0 = init_bunch(setup)
    RF_Track;
    
    mass = setup.mass; % MeV/c^2
    Q = setup.Q; % single-particle charge, in units of e
    population = setup.population; % 50 * RF_Track.pC; % number of real particles per bunch
    P_i = setup.P_i; % MeV/c
    P_f = setup.P_f; % MeV/c
    
    RF = init_rf_structure();
    L_RF = RF.get_length();
    
    %% FODO cell parameters
    mu = setup.mu; % deg
    Lquad = 0.1; % m
    Lcell = 8*L_RF + 2*Lquad; % m, eight structures and two quadrupoles
    
    %% Define Twiss parameters
    Twiss = Bunch6d_twiss();
    Twiss.emitt_x = 5; % mm.mrad, normalized emittances
    Twiss.emitt_y = 5; % mm.mrad
    Twiss.beta_x = Lcell * (1 - sind(mu/2)) / sind(mu); % m
    Twiss.beta_y = Lcell * (1 + sind(mu/2)) / sind(mu); % m
    Twiss.sigma_t = setup.sigma_t;
    Twiss.sigma_pt = setup.sigma_pt;
    
    %% Create the bunch
    B0 = Bunch6d_QR (mass, population, Q, P_i, Twiss, 1000);