function P0 = init_reference_particle(setup)
RF_Track;

P0 = Bunch6d (setup.mass, setup.population, setup.Q, [ 0 0 0 0 0 setup.P_i ]);
