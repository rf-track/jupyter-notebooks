function RF = init_rf_structure()
    RF_Track;

    T = load('data/TWS_Xband.dat');
    
    Ez = T(:,2) + 1j * T(:,3); % MV/m
    hz = T(2,1) - T(1,1); % m
    
    freq = 11.9942e9; % Hz
    phid = 0; % degrees

    E_map = 100e6; % V/m, the gradient of the field map
    E_actual = 80e6; % V/m, our target gradient

    P_map = 37.5e6; % W, the field map was generated assuming 37.5 MW input power, to provide 100 MV/m gradient
    P_actual = P_map * E_actual^2/E_map^2; % W, we want to operate at 80 MV/m

    RF = RF_FieldMap_1d (Ez, hz, -1, freq, +1, P_map, P_actual);
    RF.set_phid (phid);
    RF.set_nsteps (100);

    % Uncomment to be a little faster, using a constant Ez field.
    %RF = Drift( RF.get_length() );
    %RF.set_static_Efield (0, 0, - 0.8 * E_actual);
    
    % add SRWF to structure
    SRWF = init_SRWF();

    RF.add_collective_effect(SRWF);
    RF.set_cfx_nsteps(10);
