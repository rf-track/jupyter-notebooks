% Weigthed histogram
function [h,x] = whist (y,w,n,prop)
    if nargin < 3
        n = 10;
    end
    if isscalar(n)
        bins = linspace (min (y), max (y) + sqrt (eps), n+1);
        x =  (bins (1:n) + bins (2:n+1)) / 2;
    else
        bins = min (n) + (n(2) - n(1)) * (-0.5 + (0:length (n)));
        x = n;
        n = length (n);
    end
    h = [];
    y = y(:);
    w = w(:);
    for k = 1:n
        inbin = find (and (y>=bins (k), y<bins (k+1)));
        h = [h; sum(w(inbin)) ];
    end
    if and(nargin>=4, sum(h)!=0)
        h *= prop / sum(h);
    end
end