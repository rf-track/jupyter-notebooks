function LINAC = init_linac_lattice_bba (setup, mu)
    RF_Track;

    mass = setup.mass; % MeV/c^2
    Q = setup.Q; % single-particle charge, in units of e
    population = setup.population; % 50 * RF_Track.pC; % number of real particles per bunch
    P_i = setup.P_i % MeV/c
    P_f = setup.P_f % MeV/c
    
    RF = init_rf_structure();
    RF.set_phid(setup.phid);
    
    %% FODO cell parameters
    mu = setup.mu; % deg
    
    L_RF = RF.get_length();
    L_quad = 0.1; % m
    L_cell = 8*L_RF + 2*L_quad; % m, eight structures and two quadrupoles
    k1L = sind(mu/2) / (L_cell/4); % 1/m, integrateg focusing strength

    C = Corrector();
    B = Bpm();
    
    FODO = Lattice();
    FODO.append ( RF );
    FODO.append ( RF );
    FODO.append ( RF );
    FODO.append ( RF );
    FODO.append ( C );
    FODO.append ( Quadrupole (L_quad, 0.0 ));
    FODO.append ( B );
    FODO.append ( RF );
    FODO.append ( RF );
    FODO.append ( RF );
    FODO.append ( RF );
    FODO.append ( C );
    FODO.append ( Quadrupole (L_quad, 0.0 ));
    FODO.append ( B );
    
    % Define the reference particle
    P0 = Bunch6d (RF_Track.electronmass, population, Q, [ 0 0 0 0 0 P_i ]);
    
    % We use autophase to set the phases, and to determine P_max, the maximum final momentum
    P_max = FODO.autophase (P0);    % MeV/c
    FODO.unset_t0();
    
    % The momentum gain per FODO cell is
    P_gain = P_max - P_i;  % MeV/c
    
    n_FODO = (P_f - P_i) / P_gain;
    n_FODO = round(n_FODO)    % let's round it to the nearest integer
    
    % Start a new lattice
    LINAC = Lattice();
    
    % 1/2 quad, let's start with half a quad
    LINAC.append (C);
    LINAC.append (Quadrupole (L_quad/2, 0.0) );
    LINAC.append (B);
    
    % let's put out n_FODO cells
    for i=1:n_FODO
        LINAC.append (FODO);
    end
    
    P1 = LINAC.track(P0);
    
    Quads = LINAC.get_quadrupoles();
    
    k1 = k1L / L_quad; % 1/m^2
    
    half_P_gain = 0.5 * P_gain;

    P = P_i; % initial momentum
    for q = Quads

        % set quadrupole strength
        q{1}.set_K1 (P/Q, k1);
        
        % update the momentum variable
        P += half_P_gain;         % 

        % changes the sign of k1, anticipating the next quadrupole
        k1 = -k1;
        
    end