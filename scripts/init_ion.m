function ion = init_ion()
    RF_Track;
    
    A = 12; % Carbon
    Z = 6;
    mass = A*RF_Track.protonmass;
    
    E_kin = 430; % [MeV/u]
    E_kin_tot = A*E_kin;
    E_tot = E_kin_tot + mass;
    pc_tot = sqrt(E_tot^2 - mass^2);
    P_over_q = pc_tot / Z; % MV/c
    B_rho = P_over_q / 299.792458; % T*m
    V_tot = pc_tot / hypot(mass, pc_tot); % c, ion velocity
    
    ion.mass = mass;
    ion.Z = Z;
    ion.P_ref = pc_tot;
    ion.V_ref = V_tot;
    ion.B_rho = B_rho;
    ion.P_over_q = P_over_q;
    ion.E_tot = E_tot;
