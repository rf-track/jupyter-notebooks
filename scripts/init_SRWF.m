function SRWF = init_SRWF()
    RF_Track;
    
    freq = 11.9942e9; % Hz

    w = RF_Track.clight / freq; % m, RF wavelength
    t = 0.5 * (1.67 + 1) / 1e3; % m, average iris thickness
    a = 0.5 * (6.3 + 4.7) / 1e3 * 0.5; % m, average iris aperture radius
    l = w / 3; % m, cell length, as this is a 2pi/3 TW structure
    g = l - t; % m, gap length

    SRWF = ShortRangeWakefield(a, g, l); % a,g,l
